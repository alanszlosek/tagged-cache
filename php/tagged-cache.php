<?php

class TaggedCache {
    public function __construct($redis, $ttl = 60) {
        $this->redis = $redis;
        $this->ttl = $ttl;

        $this->namespace = 'tc:';
        $this->itemContentKeyPrefix = 'item-content:';
        $this->itemTagKeyPrefix = 'item-tags:';
        $this->tagKeyPrefix = 'tag:';
    }


    // Give each script a consistent label so we can find its sha
    protected function loadScript($id, $path) {
        $sha = $this->redis->get($this->namespace . 'script:' . $id);
        if (!$sha) {
            $sha = $this->redis->script('load', file_get_contents(__DIR__ . '/../redis-lua/' . $path));
            $this->redis->set($this->namespace . 'script:' . $id, $sha);
        }
        return $sha;
    }

    public function get($key) {
        return $this->redis->get($this->namespace . $this->itemContentKeyPrefix . $key);
    }

    public function set($key, $content, $tags = array(), $ttl = null) {
        // Make sure script is there
        $sha = $this->loadScript('cache_set', 'set.lua');

        $args = [
            $sha,
            count($tags) + 5,
            $this->namespace,
            $this->itemContentKeyPrefix,
            $this->itemTagKeyPrefix,
            $this->tagKeyPrefix,
            $key
        ];
        $args = array_merge($args, $tags);
        array_push($args, $content, time(), $ttl ?: $this->ttl);

        $ret = call_user_func_array( array($this->redis, 'evalsha'), $args);
        return $ret;
    }

    public function invalidate($tags) {
        // Make sure script is there
        $sha = $this->loadScript('cache_invalidate', 'invalidate.lua');

        $args = [
            $sha,
            count($tags) + 4,
            $this->namespace,
            $this->itemContentKeyPrefix,
            $this->itemTagKeyPrefix,
            $this->tagKeyPrefix
        ];
        $args = array_merge($args, $tags);
        //array_push($args, time(), $this->ttl);

        $ret = call_user_func_array( array($this->redis, 'evalsha'), $args);
        return $ret;
    }
}

