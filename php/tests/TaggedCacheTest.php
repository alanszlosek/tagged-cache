<?php
require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../tagged-cache.php');

$nl = "\n";

class TaggedCacheTests extends \PHPUnit_Framework_TestCase
{
    protected $redis;

    protected $ttl = 3600;

    protected function setUp() {
        $this->redis = new Predis\Client(['host' => 'redis']);
        $this->redis->select(1);
        $this->redis->flushdb();

        $this->cache = new TaggedCache($this->redis, $this->ttl);


        $this->content = 'Sample content ' . uniqid();

    }

    protected function tearDown() {
    }

    public function testAddWithoutTags()
    {
        $key = 'key-' . uniqid();
        $this->cache->set(
            $key,
            $this->content
        );

        $this->assertEquals($this->content, $this->cache->get($key));
    }

    public function testAddWithTags() {
        $key = 'key-' . uniqid();
        $this->cache->set(
            $key,
            $this->content,
            ['event-1']
        );

        $this->assertEquals($this->content, $this->cache->get($key));

        // Fetch tag sorted set and make sure content id is there
    }

    public function testInvalidate() {
        $key = 'key-' . uniqid();
        $this->cache->set(
            $key,
            $this->content,
            ['event-1']
        );

        $this->assertEquals($this->content, $this->cache->get($key));

        $this->cache->invalidate(
            ['event-1']
        );

        $this->assertEquals('', $this->cache->get($key));
    }

    public function testUpdateAndInvalidate() {
        $key = 'key-' . uniqid();

        $this->cache->set(
            $key,
            $this->content,
            ['event-1']
        );
        $this->assertEquals($this->content, $this->cache->get($key));

        $ret = $this->cache->set(
            $key,
            $this->content,
            ['event-2']
        );
        $this->assertEquals($this->content, $this->cache->get($key));

        $tags = $this->cache->invalidate(
            ['event-1']
        );
        $this->assertEquals($this->content, $this->cache->get($key));
        return;

        $tags = $this->cache->invalidate(
            ['event-2']
        );
        $this->assertEquals('', $this->cache->get($key));
    }




}

