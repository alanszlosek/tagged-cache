var fs = require('fs');


var Cache = function(redis, ttl, keyNamespace) {
    this.redis = redis;
    this.ttl = ttl;

    this.namespace = 'tc:';
    this.itemContentKeyPrefix = 'item-content:';
    this.itemTagKeyPrefix = 'item-tags:';
    this.tagKeyPrefix = 'tag:';
};

Cache.prototype.loadScript = function(id, callback) {
    var self = this;
    this.redis.get('script:' + id, function(err, sha) {
        if (err) {
            callback(err);
            return;
        }
        if (sha) {
            callback(null, sha);
            return;
        }

        var script = fs.readFileSync(id + '.lua');

        self.redis.script('load', script, function(err, sha) {
            if (err) {
                callback(err);
                return;
            }
            self.redis.set('script:' + id, sha, function(err, result) {
                if (err) {
                    callback(err);
                    return;
                }
                callback(null, sha);
            });
        });
    });
};

Cache.prototype.get = function(key, callback) {
    this.redis.get(this.namespace + this.itemContentKeyPrefix + key, function(err, value) {
        if (err) {
            callback(err);
            return;
        }
        callback(null, value);
    });
};

Cache.prototype.set = function(key, content, tags, ttl, callback) {
    var self = this;
    this.loadScript('set', function(err, sha) {
        // args = [sha, tags.length + 1, key, tags..., content, timestamp, ttl]
        var args = tags.slice();
        args.unshift(key);
        args.unshift(self.tagKeyPrefix);
        args.unshift(self.itemTagKeyPrefix);
        args.unshift(self.itemContentKeyPrefix);
        args.unshift(self.namespace);
        args.unshift(tags.length + 5);
        args.unshift(sha);
        // Push unix timestamp
        args.push(content);
        args.push(Date.now());
        args.push(ttl || self.ttl);

        self.redis.evalsha(args, function(err, result) {
            if (err) {
                // TODO: handle or return error, once we add error returns to our lua script
                callback(err);
                return;
            }
            callback(null);
        });
    });
};

Cache.prototype.invalidate = function(tags, callback) {
    var self = this;
    this.loadScript('invalidate', function(err, sha) {
        var args = tags.slice();
        args.unshift(self.tagKeyPrefix);
        args.unshift(self.itemTagKeyPrefix);
        args.unshift(self.itemContentKeyPrefix);
        args.unshift(self.namespace);
        args.unshift(tags.length + 4);
        args.unshift(sha);

        self.redis.evalsha(args, function(err, result) {
            if (err) {
                // TODO: handle or return error, once we add error returns to our lua script
                callback(err);
                return;
            }
            callback(null);
        });
    });
};

module.exports = Cache;
