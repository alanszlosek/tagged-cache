var tap = require('tap');

var TaggedCache = require('../index');
var redis = require('redis');

tap.test('set cache', function(test) {
    var redisClient = this.redisClient = redis.createClient(6379, 'redis');
    var done = function() {
        redisClient.quit();
    };
    test.plan(1);

    redisClient.on("error", function (err) {
        throw err;
    });

    var tc = new TaggedCache(redisClient, 60, 'test');
    var html = 'HTML';

    tc.set('/', html, ['homepage'], 60, function(err) {
        if (err) {
            throw err;
        }

        tc.get('/', function(err, contents) {
            if (err) {
                throw err;
            }
            test.equal(contents, html, 'Cache contains expected value');
            done();
        });
    });
});


tap.test('invalidate by tag', function(test) {
    var redisClient = this.redisClient = redis.createClient(6379, 'redis');
    var done = function() {
        redisClient.quit();
    };
    test.plan(1);

    redisClient.on("error", function (err) {
        throw err;
    });

    var tc = new TaggedCache(redisClient, 60, 'test');
    var key = '/test';
    var html = 'HTML 2';

    tc.set(key, html, ['homepage'], 60, function(err) {
        if (err) {
            throw err;
        }

        tc.invalidate(['homepage'], function(err) {
            if (err) {
                throw err;
            }
            
            tc.get(key, function(err, contents) {
                if (err) {
                    throw err;
                }
                test.equal(contents, null, 'Cache is empty');
                done();
            });
        });
    });
});

