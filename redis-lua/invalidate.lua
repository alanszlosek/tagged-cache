-- KEYS passed into lua script:
-- namespace itemContentKeyPrefix itemTagsKeyPrefix tagKeyPrefix
-- tag-1 tag-2

local namespace = table.remove(KEYS, 1)
local itemContentKeyPrefix = namespace .. table.remove(KEYS, 1)
local itemTagsKeyPrefix = namespace .. table.remove(KEYS, 1)
local tagKeyPrefix = namespace .. table.remove(KEYS, 1)


for i=1,#KEYS do
    -- get content ids
    local ids = redis.call('zrange', tagKeyPrefix .. KEYS[i], 0, -1);
    -- delete content and tags associated with those content ids
    for j=1,#ids do
        redis.call('del', itemContentKeyPrefix .. ids[j], itemTagsKeyPrefix .. ids[j])
    end

    -- delete the tag's sorted set (which contains content ids)
    redis.call('del', tagKeyPrefix .. KEYS[i])
end

