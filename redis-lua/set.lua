-- KEYS passed into lua script:
-- namespace itemContentKeyPrefix itemTagsKeyPrefix tagKeyPrefix contentId tag-1 [tag-2 ...]
-- ARGS passed into lua script:
-- contentData currentTimestamp secondsTTL

local namespace = table.remove(KEYS, 1)
local itemContentKeyPrefix = namespace .. table.remove(KEYS, 1)
local itemTagsKeyPrefix = namespace .. table.remove(KEYS, 1)
local tagKeyPrefix = namespace .. table.remove(KEYS, 1)
local contentId = table.remove(KEYS, 1)

-- store content and set its expiration
redis.call('setex', itemContentKeyPrefix .. contentId, ARGV[3], ARGV[1])

-- use new tags for this contentId, remove old associations
local key = itemTagsKeyPrefix .. contentId
local tempTags1 = 'tempTags1'
local tempTags2 = 'tempTags2'

-- union new and old tags so we can take a diff
if #KEYS > 0 then
    redis.call('del', tempTags1)
    redis.call('sadd', tempTags1, unpack(KEYS))
    redis.call('sunionstore', tempTags2, key, tempTags1)
end

-- get old tags that contentId is no longer associated with
local tags = redis.call('sdiff', tempTags2, tempTags1)

-- remove contentId from those old tags
for i=1,#tags do
    redis.call('zrem', tagKeyPrefix .. tags[i], contentId)
end

-- rename temp_keys to official key set
if #KEYS > 0 then
    redis.call('rename', tempTags1, key)
    redis.call('expire', key, ARGV[3])
end

-- ensure contentId is added to new tags
for i=1,#KEYS do
    redis.call('zadd', tagKeyPrefix .. KEYS[i], ARGV[2] + ARGV[3], contentId)
    redis.call('expire', tagKeyPrefix .. KEYS[i], ARGV[3])

    -- prune old items from zset, anything between a timestamp of 0 and (currentTimestamp - contentTTL)
    redis.call('zremrangebyscore', tagKeyPrefix .. KEYS[i], 0, ARGV[2] - ARGV[3])
end

return {KEYS[1]}
