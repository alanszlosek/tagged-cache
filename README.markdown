WELCOME
====

Problem: You want to cache your generated web pages (to give your webserver CPUs and database a break), you've got auto-scaled webservers, you don't want to use a caching reverse-proxy that doesn't scale horizontally easily/cheaply

Solution: Use a tag-based caching tool that leverages Redis for it's speed and atomicity. Use the binding for your language and insert it as middleware or integrate with your framework's route handler.

This project consists of Lua scripts for Redis, and a sample PHP class to get you started. I plan to write a node.js module later.

TODO
====

* Namespace and composer package for PHP module

RUNNING TESTS
====

phpunit tests/TaggedCacheTest.php

LICENSE
====

MIT. See LICENSE.txt for more information
